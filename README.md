## Description
WAZZUP Test Project with Nest.js, Node.js, Postgres and Jest.js

## Prerequisites
1. installed node.js >= lts/erbium
3. postgresql12 database with enabled "uuid-ossp" extension

WAZZUP test project

## Installation

```bash
$ npm install
```

## Running the app

1. edit .env file from example by adding database credentials and so on

```bash

# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# test coverage
$ npm run test:cov
```

### To see the swagger docs just go to [localhost:5000/api/docs/](localhost:5000/api/docs/)

## Support
Telegram: _@kylych_
