import {
  Body,
  Controller,
  Delete,
  Get,
  Logger,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Put,
  Query,
  Req,
  UseGuards,
  UsePipes,
} from '@nestjs/common';
import {
  ApiBody,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiParam,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { NoteService } from './note.service';
import { NoteDTO, NoteRO } from './note.dto';
import { User } from '../user/user.decorator';
import { SharedNoteDTO, SharedNoteRO } from './shared-note.dto';
import { Request } from 'express';
import { AuthGuard } from '../shared/auth.guard';
import { ValidationPipe } from '../shared/validation.pipe';

@Controller('note')
@ApiTags('notes')
@UsePipes(new ValidationPipe())
export class NoteController {
  constructor(private noteService: NoteService) {}
  private logger = new Logger('NoteController');

  private logData(options: any) {
    options.body && this.logger.log('Body ' + JSON.stringify(options.body));
    options.id && this.logger.log('Note ' + JSON.stringify(options.id));
  }

  @Post()
  @ApiBody({
    description: 'Data for note creation.',
    type: NoteDTO,
  })
  @ApiCreatedResponse({
    description: 'Note response.',
    type: NoteRO,
  })
  @UseGuards(new AuthGuard())
  createNote(
    @User('id') userId: number,
    @Body() body: NoteDTO,
  ): Promise<NoteRO> {
    this.logData({ body });
    return this.noteService.create(userId, body);
  }

  @Get(':id')
  @ApiParam({
    name: 'id',
    type: Number,
  })
  @ApiOkResponse({
    description: 'Note response.',
    type: NoteRO,
  })
  @UseGuards(new AuthGuard())
  retrieveNote(
    @User('id') userId: number,
    @Param('id', ParseIntPipe) id: number,
  ): Promise<NoteRO> {
    this.logData({ id });
    return this.noteService.retrieve(userId, id);
  }

  @Put(':id')
  @ApiParam({
    name: 'id',
    type: Number,
  })
  @ApiOkResponse({
    description: 'Note response.',
    type: NoteRO,
  })
  @ApiBody({
    description: 'Data for note update.',
    type: NoteDTO,
  })
  @UseGuards(new AuthGuard())
  updateNote(
    @User('id') userId: number,
    @Param('id', ParseIntPipe) id: number,
    @Body() body: Partial<NoteDTO>,
  ): Promise<NoteRO> {
    this.logData({ id, body });
    return this.noteService.update(userId, id, body);
  }

  @Delete(':id')
  @ApiParam({
    name: 'id',
    type: Number,
  })
  @ApiOkResponse({
    description: 'Note response.',
    type: NoteRO,
  })
  @UseGuards(new AuthGuard())
  deleteNote(
    @User('id') userId: number,
    @Param('id', ParseIntPipe) id: number,
  ): Promise<NoteRO> {
    this.logData({ id });
    return this.noteService.delete(userId, id);
  }

  @Get()
  @ApiQuery({
    name: 'page',
    type: Number,
    required: false,
  })
  @ApiOkResponse({
    description: 'Note response.',
    type: [NoteRO],
  })
  @UseGuards(new AuthGuard())
  listNotes(
    @User('id') userId: number,
    @Query('page') page = 1,
  ): Promise<NoteRO[]> {
    return this.noteService.list(userId, page);
  }

  @Post('/share')
  @ApiBody({
    description: 'Data for sharedNote creation.',
    type: SharedNoteDTO,
  })
  @ApiCreatedResponse({
    description: 'Shared note response',
    type: SharedNoteRO,
  })
  @UseGuards(new AuthGuard())
  enableSharedNote(
    @User('id') userId: number,
    @Body() body: SharedNoteDTO,
    @Req() request: Partial<Request>,
  ): Promise<SharedNoteRO> {
    return this.noteService.createSharedNote(
      body,
      userId,
      request.protocol,
      request.headers.host,
    );
  }

  @Put('/share/disable')
  @Patch('/share/disable')
  @ApiBody({
    description: 'Data for sharedNote creation.',
    type: SharedNoteDTO,
  })
  @ApiOkResponse({
    type: Object,
  })
  @UseGuards(new AuthGuard())
  disableSharedNote(
    @User('id') userId: number,
    @Body() body: SharedNoteDTO,
  ): Promise<object> {
    return this.noteService.updateSharedNote(body, userId);
  }

  @Get('/shared/:id')
  @ApiOkResponse({
    description: 'Shared note response',
    type: String,
  })
  @ApiParam({
    name: 'id',
    type: String,
  })
  showSharedNote(@Param('id') id: string): Promise<string> {
    return this.noteService.retrieveSharedNote(id);
  }
}
