import { Module } from '@nestjs/common';
import { NoteController } from './note.controller';
import { NoteService } from './note.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Note } from './note.entity';
import { User } from '../user/user.entity';
import { SharedNote } from './shared-note.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Note, User, SharedNote])],
  controllers: [NoteController],
  providers: [NoteService],
})
export class NoteModule {}
