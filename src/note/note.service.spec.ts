import { Test, TestingModule } from '@nestjs/testing';
import { NoteService } from './note.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Note } from './note.entity';
import { SharedNote } from './shared-note.entity';
import { User } from '../user/user.entity';

describe('NoteService', () => {
  let noteService: NoteService;

  const testNoteContent = 'Test Content';
  const testNoteId = 1;

  const testUserId = 2;
  const testUserId2 = 4;
  const testUser = new User('username', 'password', testUserId);

  const testNote = new Note(testNoteContent, testUser);
  const testNotes = [testNote, new Note('Test Content1', testUser)];
  testUser.notes = testNotes;

  const testNoteDTO = { content: testNoteContent };
  const testNoteRO = {
    id: testNoteId,
    content: testNoteContent,
    createdAt: new Date(),
    updatedAt: new Date(),
  };
  const testNoteRO1 = {
    id: 2,
    content: 'Test Content 2',
    createdAt: new Date(),
    updatedAt: new Date(),
  };
  const testNoteROList = [testNoteRO, testNoteRO1];

  testNote.toResponseObject = jest.fn().mockReturnValue(testNoteRO);
  testNotes.map = jest.fn().mockReturnValueOnce(testNoteROList);

  const testSharedNoteDTO = {
    noteId: testNoteId,
  };

  const mockForRetrieveUpdateDeleteNote = jest
    .fn()
    .mockReturnValueOnce(testUserId)
    .mockReturnValueOnce(testNoteId);

  const mockForCreateListNotes = jest
    .fn()
    .mockReturnValueOnce(testUserId)
    .mockReturnValueOnce(testNoteDTO);

  const testSharedNoteId = '2v356467n43c45x5435757m859';
  const testReqProtocol = 'http';
  const testReqHost = 'localhost:5000';
  const testSharedNote = new SharedNote(testNote, true, testSharedNoteId);

  const testSharedNoteURL = `${testReqProtocol}://${testReqHost}/note/shared/${testSharedNoteId}`;

  const testSharedNoteRO = {
    url: testSharedNoteURL,
  };

  const mockForSharedNoteEnableDisable = jest
    .fn()
    .mockReturnValueOnce(testSharedNoteDTO)
    .mockReturnValueOnce(testUserId)
    .mockReturnValueOnce(testReqProtocol)
    .mockReturnValueOnce(testReqHost)
    .mockReturnValueOnce(testSharedNoteRO);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        NoteService,
        {
          provide: getRepositoryToken(Note),
          useValue: {
            findOne: jest.fn().mockResolvedValue(testNote),
            create: jest.fn().mockResolvedValue(testNote),
            save: jest.fn().mockResolvedValue(testNote),
            update: jest.fn().mockResolvedValue({}),
            remove: jest.fn().mockResolvedValue(testNote),
            find: jest.fn().mockResolvedValue(testNotes),
          },
        },
        {
          provide: getRepositoryToken(SharedNote),
          useValue: {
            findOne: jest.fn().mockResolvedValue(testSharedNote),
            create: jest.fn().mockResolvedValue(testSharedNote),
            save: jest.fn().mockResolvedValue(testSharedNote),
            update: jest.fn().mockResolvedValue({}),
          },
        },
        {
          provide: getRepositoryToken(User),
          useValue: {
            findOne: jest.fn().mockResolvedValue(testUser),
          },
        },
      ],
    }).compile();

    noteService = module.get<NoteService>(NoteService);
  });

  it('should be defined', () => {
    expect(noteService).toBeDefined();
  });

  describe('create new note', () => {
    it('should create new note', () => {
      expect(
        noteService.create(mockForCreateListNotes(), mockForCreateListNotes()),
      ).resolves.toEqual(testNoteRO);
    });
  });

  describe('retrieve single note', () => {
    it('should return note', async () => {
      const spy = jest.spyOn(noteService, 'ensureOwnership');
      await expect(
        noteService.retrieve(
          testUserId,
          testNoteId
        ),
      ).resolves.toEqual(testNoteRO);
      expect(spy).toBeCalledWith(testUserId, testNote.author.id);
      expect(spy).toBeCalledTimes(1);
    });
  });

  describe('update single note', () => {
    it('should update note', async () => {
      const spy = jest.spyOn(noteService, 'ensureOwnership');
      await expect(
        noteService.update(
          testUserId,
          testNoteId,
          testNoteDTO
        ),
      ).resolves.toEqual(testNoteRO);
      expect(spy).toBeCalledWith(testUserId, testNote.author.id);
      expect(spy).toBeCalledTimes(1);
    });
  });

  describe('delete single note', () => {
    it('should delete note', async () => {
      const spy = jest.spyOn(noteService, 'ensureOwnership');
      await expect(
        noteService.delete(
          testUserId,
          testNoteId
        ),
      ).resolves.toEqual(testNoteRO);
      expect(spy).toBeCalledWith(testUserId, testNote.author.id);
      expect(spy).toBeCalledTimes(1);
    });
  });

  describe('retrieve notes list', () => {
    it('should retrieve notes list', () => {
      expect(
        noteService.list(mockForRetrieveUpdateDeleteNote()),
      ).resolves.toEqual(testNoteROList);
    });
  });

  describe('create shared note',  () => {
    it('should create or enable shared note', async () => {
      const spy = jest.spyOn(noteService, 'ensureOwnership');
      await expect(
        noteService.createSharedNote(
          mockForSharedNoteEnableDisable(),
          mockForSharedNoteEnableDisable(),
          mockForSharedNoteEnableDisable(),
          mockForSharedNoteEnableDisable(),
        ),
      ).resolves.toEqual(mockForSharedNoteEnableDisable());
      expect(spy).toBeCalledWith(testUserId, testNote.author.id);
      expect(spy).toBeCalledTimes(1);
    });
  });

  describe('disable shared note by id', () => {
    it('should disable shared note', async () => {
      const spy = jest.spyOn(noteService, 'ensureOwnership');
      await expect(
        noteService.updateSharedNote(testSharedNoteDTO, testUserId),
      ).resolves.toEqual({ disabled: true });
      expect(spy).toBeCalledWith(testUserId, testNote.author.id);
      expect(spy).toBeCalledTimes(1);
    });
  });

  describe('retrieve shared note', () => {
    it('should retrieve shared note content', () => {
      expect(noteService.retrieveSharedNote(testSharedNoteId)).resolves.toBe(
        testNoteContent,
      );
    });
  });

  describe('check note ownership', () => {
    it('should not throw error', () => {
      expect(() => {
        noteService.ensureOwnership(testUserId, testNote.author.id);
      }).not.toThrowError();
    });
    it('should throw error', () => {
      expect(() => {
        noteService.ensureOwnership(testUserId2, testNote.author.id);
      }).toThrowError();
    });
  });

  afterAll(() => {
    const spy = jest.spyOn(noteService, 'ensureOwnership');
    expect(spy).toBeCalledTimes(5);
  })
});
