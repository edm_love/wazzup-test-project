import { IsNotEmpty, IsString, MaxLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class NoteDTO {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @MaxLength(1000)
  content: string;
}

export class NoteRO {
  @ApiProperty()
  id: number;

  @ApiProperty()
  content: string;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;
}
