import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Note } from './note.entity';
import { NoteDTO, NoteRO } from './note.dto';
import { User } from '../user/user.entity';
import { SharedNote } from './shared-note.entity';
import { SharedNoteDTO, SharedNoteRO } from './shared-note.dto';
import * as url from 'url';

@Injectable()
export class NoteService {
  constructor(
    @InjectRepository(Note)
    private noteRepository: Repository<Note>,
    @InjectRepository(User)
    private userRepository: Repository<User>,
    @InjectRepository(SharedNote)
    private sharedNoteRepository: Repository<SharedNote>,
  ) {}

  async create(authorId: number, data: NoteDTO): Promise<NoteRO> {
    const author: User = await this.userRepository.findOne({
      where: {
        id: authorId,
      },
    });
    const note: Note = await this.noteRepository.create({
      ...data,
      author,
    });
    await this.noteRepository.save(note);
    return note.toResponseObject();
  }

  async retrieve(authorId: number, noteId: number): Promise<NoteRO> {
    const note: Note = await this.noteRepository.findOne({
      where: { id: noteId },
      relations: ['author'],
    });
    if (!note) {
      throw new NotFoundException();
    }
    this.ensureOwnership(authorId, note.author.id);
    return note.toResponseObject();
  }

  async update(
    authorId: number,
    noteId: number,
    data: Partial<NoteDTO>,
  ): Promise<NoteRO> {
    let note: Note = await this.noteRepository.findOne({
      where: { noteId },
      relations: ['author'],
    });
    if (!note) {
      throw new NotFoundException();
    }
    this.ensureOwnership(authorId, note.author.id);
    await this.noteRepository.update({ id: noteId }, data);
    note = await this.noteRepository.findOne({
      where: { id: noteId },
    });
    return note.toResponseObject();
  }

  async delete(authorId: number, noteId: number): Promise<NoteRO> {
    const note: Note = await this.noteRepository.findOne({
      where: { id: noteId },
      relations: ['author'],
    });
    if (!note) {
      throw new NotFoundException();
    }
    this.ensureOwnership(authorId, note.author.id);
    await this.noteRepository.remove(note);
    return note.toResponseObject();
  }

  async list(authorId: number, page = 1): Promise<NoteRO[]> {
    const notes: Note[] = await this.noteRepository.find({
      where: { author: { id: authorId } },
      take: 25,
      skip: 25 * (page - 1),
    });
    return notes.map(note => note.toResponseObject());
  }

  ensureOwnership(userId: number, exactAuthorId: number): void {
    if (exactAuthorId !== userId) {
      throw new NotFoundException();
    }
  }

  async createSharedNote(
    data: SharedNoteDTO,
    authorId: number,
    reqProtocol: string,
    reqHost: string,
  ): Promise<SharedNoteRO> {
    const note: Note = await this.noteRepository.findOne({
      where: { id: data.noteId },
      relations: ['author'],
    });
    if (!note) {
      throw new NotFoundException();
    }
    this.ensureOwnership(authorId, note.author.id);
    let sharedNote: SharedNote = await this.sharedNoteRepository.findOne({
      where: { note: { id: note.id } },
    });
    if (!sharedNote) {
      sharedNote = await this.sharedNoteRepository.create({
        note,
      });
      await this.sharedNoteRepository.save(sharedNote);
    } else {
      await this.sharedNoteRepository.update(
        { id: sharedNote.id },
        { isActive: true },
      );
    }
    return {
      url: url.resolve(
        `${reqProtocol}://${reqHost}`,
        `/note/shared/${sharedNote.id}`,
      ),
    };
  }

  async updateSharedNote(
    data: SharedNoteDTO,
    authorId: number,
  ): Promise<object> {
    const note: Note = await this.noteRepository.findOne({
      where: {
        id: data.noteId,
      },
      relations: ['author'],
    });
    if (!note) {
      throw new NotFoundException();
    }
    this.ensureOwnership(authorId, note.author.id);
    const sharedNote: SharedNote = await this.sharedNoteRepository.findOne({
      where: {
        note: { id: note.id },
      },
    });
    if (sharedNote && sharedNote.isActive) {
      sharedNote.isActive = false;
      await this.sharedNoteRepository.save(sharedNote);
    } else {
      return {
        message: 'Shared note not found or already is disabled.',
      };
    }
    return {
      disabled: true,
    };
  }

  async retrieveSharedNote(sharedNoteId: string): Promise<string> {
    const sharedNote: SharedNote = await this.sharedNoteRepository.findOne({
      where: {
        id: sharedNoteId,
        isActive: true,
      },
      relations: ['note'],
    });
    if (!sharedNote) {
      throw new NotFoundException();
    }
    return sharedNote.note.content;
  }
}
