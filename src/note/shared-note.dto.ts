import { IsInt, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class SharedNoteDTO {
  @ApiProperty()
  @IsNotEmpty()
  @IsInt()
  noteId: number;
}

export class SharedNoteRO {
  @ApiProperty()
  url: string;
}
