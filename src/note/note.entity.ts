import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
} from 'typeorm';
import { MaxLength } from 'class-validator';
import { User } from '../user/user.entity';
import { NoteRO } from './note.dto';

@Entity()
export class Note {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({
    type: 'varchar',
    length: 1000,
  })
  @MaxLength(1000)
  content: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @ManyToOne(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    type => User,
    author => author.notes,
  )
  author: User;

  constructor(
    content: string,
    author: User,
    id?: number,
    createdAt?: Date,
    updatedAt?: Date,
  ) {
    this.content = content;
    this.author = author;
    this.id = id || NaN;
    this.createdAt = createdAt || new Date();
    this.updatedAt = updatedAt || new Date();
  }

  toResponseObject(): NoteRO {
    return {
      id: this.id,
      content: this.content,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
    };
  }
}
