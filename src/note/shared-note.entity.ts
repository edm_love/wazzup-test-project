import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { Note } from './note.entity';

@Entity()
export class SharedNote {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @Column({
    type: 'boolean',
    default: true,
  })
  isActive: boolean;

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @OneToOne(type => Note)
  @JoinColumn()
  note: Note;

  constructor(note: Note, isActive?: boolean, id?: string) {
    this.note = note;
    this.isActive = isActive || true;
    this.id = id || '';
  }
}
