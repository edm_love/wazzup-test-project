import { Test, TestingModule } from '@nestjs/testing';
import { NoteController } from './note.controller';
import { NoteService } from './note.service';

describe('User Controller', () => {
  let noteController: NoteController;

  const testNoteContent = 'Test Content';
  const testNoteId = 1;
  const testNoteCreatedAt = new Date();
  const testNoteUpdatedAt = new Date();

  const testNoteDTO = { content: testNoteContent };
  const testNoteRO = {
    id: testNoteId,
    content: testNoteContent,
    createdAt: testNoteCreatedAt,
    updatedAt: testNoteUpdatedAt,
  };
  const testNoteRO1 = {
    id: 2,
    content: 'Test Content 2',
    createdAt: new Date(),
    updatedAt: new Date(),
  };
  const testNoteROList = [testNoteRO, testNoteRO1];

  const testUserId = 2;

  const testSharedNoteDTO = {
    noteId: testNoteId,
  };

  const testSharedNoteId = '2v356467n43c45x5435757m859';
  const testReqProtocol = 'http';
  const testReqHost = 'localhost:5000';
  const testRequestObject = {
    protocol: testReqProtocol,
    headers: {
      host: testReqHost,
    },
  };

  const testSharedNoteURL = `${testReqProtocol}://${testReqHost}/note/shared/${testSharedNoteId}`;

  const testSharedNoteRO = {
    url: testSharedNoteURL,
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [NoteController],
      providers: [
        {
          provide: NoteService,
          useValue: {
            create: jest.fn().mockResolvedValue(testNoteRO),
            retrieve: jest.fn().mockResolvedValue(testNoteRO),
            update: jest.fn().mockResolvedValue(testNoteRO),
            delete: jest.fn().mockResolvedValue(testNoteRO),
            list: jest.fn().mockResolvedValue(testNoteROList),
            createSharedNote: jest.fn().mockResolvedValue(testSharedNoteRO),
            updateSharedNote: jest.fn().mockResolvedValue({
              disabled: true,
            }),
            retrieveSharedNote: jest
              .fn()
              .mockResolvedValue(testNoteContent),
          },
        },
      ],
    }).compile();

    noteController = module.get<NoteController>(NoteController);
  });

  it('should be defined', () => {
    expect(noteController).toBeDefined();
  });

  describe('create new note', () => {
    it('should create new note', () => {
      expect(
        noteController.createNote(
          testUserId,
          testNoteDTO
        ),
      ).resolves.toEqual(testNoteRO);
    });
  });

  describe('retrieve note by id', () => {
    it('should retrieve note', () => {
      expect(
        noteController.retrieveNote(
          testUserId,
          testNoteId
        ),
      ).resolves.toEqual(testNoteRO);
    });
  });

  describe('update note by id', () => {
    it('should update note', () => {
      expect(
        noteController.updateNote(
          testUserId,
          testNoteId,
          testNoteDTO
        ),
      ).resolves.toEqual(testNoteRO);
    });
  });

  describe('delete note by id', () => {
    it('should delete note', () => {
      expect(
        noteController.deleteNote(
          testUserId,
          testNoteId,
        ),
      ).resolves.toEqual(testNoteRO);
    });
  });

  describe("list all user's notes with pagination", () => {
    it('should retrieve notes list', () => {
      expect(
        noteController.listNotes(testUserId),
      ).resolves.toEqual(testNoteROList);
    });
  });

  describe('enable shared note by id', () => {
    it('should enable shared note', () => {
      expect(
        noteController.enableSharedNote(
          testUserId,
          testSharedNoteDTO,
          testRequestObject
        ),
      ).resolves.toEqual(testSharedNoteRO);
    });
  });

  describe('disable shared note by id', () => {
    it('should disable shared note', () => {
      expect(
        noteController.disableSharedNote(testUserId, testSharedNoteDTO),
      ).resolves.toEqual({ disabled: true });
    });
  });

  describe('view shared note by id', () => {
    it('should retrieve shared note content', () => {
      expect(noteController.showSharedNote(testSharedNoteId)).resolves.toBe(
        testNoteContent,
      );
    });
  });
});
