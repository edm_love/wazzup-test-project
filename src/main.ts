import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

require('dotenv').config();

async function bootstrap() {
  const port: number = +process.env.PORT || 5000;
  const app = await NestFactory.create(AppModule);
  const options = new DocumentBuilder()
    .setTitle('Level Test Constructor API')
    .setDescription('Level Test Constructor API')
    .setVersion('1.0.0')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api/docs', app, document);
  await app.listen(port);
}

bootstrap();
