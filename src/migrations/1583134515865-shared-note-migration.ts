import { MigrationInterface, QueryRunner } from 'typeorm';

export class sharedNoteMigration1583134515865 implements MigrationInterface {
  name = 'sharedNoteMigration1583134515865';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "shared_note" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "isActive" boolean NOT NULL DEFAULT true, "noteId" integer, CONSTRAINT "REL_dc3a11d4963d12fc1594eaf542" UNIQUE ("noteId"), CONSTRAINT "PK_99c62b91170242dd70df9b7a28d" PRIMARY KEY ("id"))`,
      undefined,
    );
    await queryRunner.query(
      `ALTER TABLE "shared_note" ADD CONSTRAINT "FK_dc3a11d4963d12fc1594eaf5423" FOREIGN KEY ("noteId") REFERENCES "note"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
      undefined,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "shared_note" DROP CONSTRAINT "FK_dc3a11d4963d12fc1594eaf5423"`,
      undefined,
    );
    await queryRunner.query(`DROP TABLE "shared_note"`, undefined);
  }
}
