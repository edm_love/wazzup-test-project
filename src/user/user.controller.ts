import { Body, Controller, Post, UsePipes } from '@nestjs/common';
import { UserService } from './user.service';
import { UserDTO, UserRO } from './user.dto';
import {
  ApiBody,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';
import { ValidationPipe } from '../shared/validation.pipe';

@Controller()
@ApiTags('auth')
@UsePipes(new ValidationPipe())
export class UserController {
  constructor(private userService: UserService) {}

  @Post('/auth/sign-up')
  @ApiBody({
    description: 'Data for user sign-up.',
    type: UserDTO,
  })
  @ApiCreatedResponse({
    description: 'Token response.',
    type: UserRO,
  })
  @UsePipes(new ValidationPipe())
  signUp(@Body() data: UserDTO) {
    return this.userService.processRegister(data);
  }

  @Post('/auth/sign-in')
  @ApiBody({
    description: 'Data for user sign-in.',
    type: UserDTO,
  })
  @ApiOkResponse({
    description: 'Token response.',
    type: UserRO,
  })
  @UsePipes(new ValidationPipe())
  signIn(@Body() data: UserDTO) {
    return this.userService.processLogin(data);
  }
}
