import { Test, TestingModule } from '@nestjs/testing';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { UserDTO } from './user.dto';
import { User } from './user.entity';

describe('User Controller', () => {
  let userController: UserController;

  const testUsername = 'test_user1';
  const testPassword = 'password1';

  const testUser: UserDTO = new User(testUsername, testPassword);
  const testUserTokenResponse = { token: 'test_token' };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [
        {
          provide: UserService,
          useValue: {
            processLogin: jest
              .fn()
              .mockResolvedValueOnce(testUserTokenResponse),
            processRegister: jest
              .fn()
              .mockResolvedValueOnce(testUserTokenResponse),
          },
        },
      ],
    }).compile();

    userController = module.get<UserController>(UserController);
  });

  it('should be defined', () => {
    expect(userController).toBeDefined();
  });

  describe('signUp  user', () => {
    it('should signUp new user', () => {
      expect(userController.signUp(testUser)).resolves.toEqual(
        testUserTokenResponse,
      );
    });
  });

  describe('signIn user', () => {
    it('should signIn user', () => {
      expect(userController.signIn(testUser)).resolves.toEqual(
        testUserTokenResponse,
      );
    });
  });
});
