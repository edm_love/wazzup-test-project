import { Test, TestingModule } from '@nestjs/testing';
import { UserService } from './user.service';
import { User } from './user.entity';
import { getRepositoryToken } from '@nestjs/typeorm';

describe('UserService', () => {
  let userService: UserService;

  const testUserName = 'username';
  const testPassword = 'password';
  const testUserDTO = {
    username: testUserName,
    password: testPassword,
  };
  const testUser = new User(testUserName, testPassword);

  const testToken = '4v346bn8mtnbvcacasv7bdnf';
  const testUserRO = { token: testToken };
  testUser.toTokenResponseObject = jest.fn().mockResolvedValue(testUserRO);

  const findOneMock = jest
    .fn()
    .mockResolvedValueOnce(undefined)
    .mockResolvedValueOnce(testUser);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserService,
        {
          provide: getRepositoryToken(User),
          useValue: {
            findOne: findOneMock,
            create: jest.fn().mockResolvedValueOnce(testUser),
            save: jest.fn().mockResolvedValueOnce(testUser),
          },
        },
      ],
    }).compile();

    userService = module.get<UserService>(UserService);
  });

  it('should be defined', () => {
    expect(userService).toBeDefined();
  });

  describe('user registration', () => {
    it('should register new user and generate jwt token', () => {
      expect(userService.processRegister(testUserDTO)).toEqual(
        testUser.toTokenResponseObject(),
      );
    });

    it('should throw error', async () => {
      await expect(userService.processRegister(testUserDTO)).rejects.toEqual(
        new Error('Username has already taken.'),
      );
    });
  });

  describe('user login', () => {
    it('should throw error because of wrong password', async () => {
      testUser.comparePassword = jest
        .fn()
        .mockResolvedValueOnce(false);
      await expect(userService.processLogin(testUserDTO)).rejects.toEqual(
        new Error('Invalid username/password.'),
      );
    });

    it('should throw error because of wrong username', async () => {
      testUser.comparePassword = jest
        .fn()
        .mockResolvedValueOnce(true);
      await expect(userService.processLogin(testUserDTO)).rejects.toEqual(
        new Error('Invalid username/password.'),
      );
    });

    it('should check user credentials and generate jwt token', () => {
      testUser.comparePassword = jest
        .fn()
        .mockResolvedValueOnce(true);
      expect(userService.processRegister(testUserDTO)).toEqual(
        testUser.toTokenResponseObject(),
      );
    });
  });
});
