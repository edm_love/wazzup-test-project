if (process.env.NODE_ENV === 'dev') {
   require('dotenv').config();
}

module.exports = {
   name: 'default',
   type: 'postgres',
   host: process.env.DATABASE_HOST,
   port: process.env.DATABASE_PORT,
   username: process.env.DATABASE_USERNAME,
   password: process.env.DATABASE_PASSWORD,
   database: process.env.DATABASE_NAME,
   synchronize: false,
   dropSchema: false,
   logging: true,
   migrationsRun: false,
   entities: ['src/**/*.entity.ts', 'dist/**/*.entity.js'],
   migrations: ['src/migrations/*.ts'],
   cli: {
      migrationsDir: 'src/migrations'
   }
};

